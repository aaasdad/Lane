package com.sc.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	public static String getFriendlyLatLon(String latlon){
		if(latlon != null){
			String pattern = "latitude:\\s*(\\d+\\.\\d+),\\s*longitude:\\s*(\\d+\\.\\d+)";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(latlon);
			if (m.find()) {
				return "���ȣ�" + m.group(2) + " γ�ȣ�" + m.group(1);
			} else {
				return latlon;
			}
		}
		else{
			return null;
		}
	}
}
