package com.sc.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class BitmapUtil {
	public static Bitmap cutterBitmap(Bitmap srcBitmap, int limitWidth,
			int limitHeight) {
		float width = srcBitmap.getWidth();
		float height = srcBitmap.getHeight();

		float limitScale = limitWidth / limitHeight;
		float srcScale = width / height;

		if (limitScale > srcScale) {
			// 高小了，所以要去掉多余的高度
			height = limitHeight / ((float) limitWidth / width);
		} else {
			// 宽度小了，所以要去掉多余的宽度
			width = limitWidth / ((float) limitHeight / height);
		}

		Bitmap bitmap = Bitmap.createBitmap((int) width, (int) height,
				Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		canvas.drawBitmap(srcBitmap, 0, 0, new Paint());
		return bitmap;
	}
}
