package com.sc.db;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

public class DBStorage {
	private static final String MARK_POINT_FOREIGN_KEY = "markRecordId";
	
	public static void createMarkPoint(Context ctx, MarkPoint point){
		DaoHelper<MarkPoint> daoHelper = 
				new DaoHelper<MarkPoint>(ctx, MarkPoint.class);
		daoHelper.create(point);
	}
	
	public static void createMarkRecord(Context ctx, MarkRecord record){
		DaoHelper<MarkRecord> daoHelper = 
				new DaoHelper<MarkRecord>(ctx, MarkRecord.class);
		daoHelper.create(record);
	}
	
	public static List<MarkRecord> getMarkRecords(Context ctx){
		DaoHelper<MarkRecord> recordDaoHelper = 
				new DaoHelper<MarkRecord>(ctx, MarkRecord.class);
		return recordDaoHelper.queryForAll();
	}
	
	public static MarkRecord getMarkRecordById(Context ctx, int id){
		DaoHelper<MarkRecord> recordDaoHelper = 
				new DaoHelper<MarkRecord>(ctx, MarkRecord.class);
		return recordDaoHelper.queryForId((long) id);
	}
	
	public static boolean updateMarkRecord(Context ctx, MarkRecord record){
		DaoHelper<MarkRecord> recordDaoHelper = 
				new DaoHelper<MarkRecord>(ctx, MarkRecord.class);
		return recordDaoHelper.update(record);
	}
	
	public static List<MarkPoint> getMarkPoints(Context ctx, int recordId){
		DaoHelper<MarkPoint> pointDaoHelper = 
				new DaoHelper<MarkPoint>(ctx, MarkPoint.class);
		return pointDaoHelper.queryForEq(MARK_POINT_FOREIGN_KEY, 
				String.valueOf(recordId));
	}
	
	public static boolean updateMarkPointPhoto(Context ctx, int pointId, String photo){
		DaoHelper<MarkPoint> pointDaoHelper = 
				new DaoHelper<MarkPoint>(ctx, MarkPoint.class);
		MarkPoint point = pointDaoHelper.queryForId((long) pointId);
		if(point != null){
			if(point.getPhotos() == null){
				point.setPhotos(photo);
			}
			else{
				point.setPhotos(point.getPhotos() + "," + photo);
			}
			
			return pointDaoHelper.update(point);
		}
		return false;
	}
	
	public static boolean removeMarkRecord(Context ctx, int recordId){
		DaoHelper<MarkRecord> recordDaoHelper = 
				new DaoHelper<MarkRecord>(ctx, MarkRecord.class);
		
		DaoHelper<MarkPoint> pointDaoHelper = 
				new DaoHelper<MarkPoint>(ctx, MarkPoint.class);
		
		MarkRecord record = recordDaoHelper.queryForId((long) recordId);
		if(record != null){
			List<MarkPoint> points = pointDaoHelper.queryForEq(MARK_POINT_FOREIGN_KEY, 
					String.valueOf(record.getId()));
			
			pointDaoHelper.delete(points);
			
			return recordDaoHelper.delete(record);
		}
		else{
			return false;
		}
	}
	
	public static boolean removeMarkPoint(Context ctx, int pointId){
		DaoHelper<MarkPoint> pointDaoHelper = 
				new DaoHelper<MarkPoint>(ctx, MarkPoint.class);
		return pointDaoHelper.deleteById((long) pointId);
	}
	
	public static int getLatestMarkRecordId(Context ctx){
		int id = 0;
		DaoHelper<MarkRecord> daoHelper = 
				new DaoHelper<MarkRecord>(ctx, MarkRecord.class);
		try {
			MarkRecord record = (MarkRecord)(daoHelper.getDao()
					.queryBuilder().orderBy("id", false).queryForFirst());
			if(record != null){
				id = record.getId();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	public static int getLatestMarkPointId(Context ctx){
		int id = 0;
		DaoHelper<MarkPoint> pointDaoHelper = 
				new DaoHelper<MarkPoint>(ctx, MarkPoint.class);
		try{
			MarkPoint point = (MarkPoint)(pointDaoHelper.getDao()
					.queryBuilder().orderBy("id", false).queryForFirst());
			if(point != null){
				id = point.getId();
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		
		return id;
	}
	
	public static String getString(Context ctx){
		StringBuilder builder = new StringBuilder();
		
		DaoHelper<MarkRecord> recordDaoHelper = 
				new DaoHelper<MarkRecord>(ctx, MarkRecord.class);
		
		DaoHelper<MarkPoint> pointDaoHelper = 
				new DaoHelper<MarkPoint>(ctx, MarkPoint.class);
		
		List<MarkRecord> records = recordDaoHelper.queryForAll();
		for(MarkRecord record : records){
			builder.append(record.toString());
			
			List<MarkPoint> points = pointDaoHelper.queryForEq("markRecordId", 
					String.valueOf(record.getId()));
			for(MarkPoint point : points){
				builder.append(point.toString());
			}
		}
		
		return builder.toString();
	}
}
